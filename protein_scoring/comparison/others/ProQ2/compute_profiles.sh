#!/usr/bin/bash

# save the list of all pdb structures
ls ../../../../datasets/CASP/data/CASP12Stage1/*/*[!txt] > list_stage_1.txt
ls ../../../../datasets/CASP/data/CASP12Stage2/*/*[!txt] > list_stage_2.txt


# iterate over representatives with full sequences
for x in ../../../../datasets/CASP/data/CASP12Stage1/*/server01_TS1
do
    ./ProQ_scripts/bin/run_all_external.pl -pdb $x -cpu 72
done

# propogate profiles ot other protein models
for x in $(cat list_stage_1.txt); do
    if [[ $(basename $x) != server01_TS1 ]]; then
        ./ProQ_scripts/bin/copy_features_from_master.pl $x $(dirname $x)/server01_TS1 &
    fi
done
wait


# propogate profiles ot other protein models
for x in $(cat list_stage_2.txt); do
    target=$(basename $(dirname $x))
    ./ProQ_scripts/bin/copy_features_from_master.pl $x $(dirname $x)/../../CASP12Stage1/$target/server01_TS1 &
done
wait
