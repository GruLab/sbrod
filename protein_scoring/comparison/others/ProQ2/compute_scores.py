#!/usr/bin/python2.7


import sys
from glob import glob
import os
import commands
from multiprocessing import Pool


def compute_scores_for_structure(filename):
    print commands.getstatusoutput('./score_structure.sh "{}"'.format(filename))[1]


def main():
    structures = []

    with open('list_stage_1.txt') as f:
        structures.extend([line.strip() for line in f])

    with open('list_stage_2.txt') as f:
        structures.extend([line.strip() for line in f])

    pool = Pool(processes=72)
    results = pool.map(compute_scores_for_structure, structures)


if __name__ == '__main__':
    main()
