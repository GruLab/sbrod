#!/usr/bin/bash


scores_filename="CASP12_ProQ3.txt"
refine_scores_filename="CASP12_ProQ3refine.txt"


for x in $(cat ../ProQ2/list_stage_1.txt) $(cat ../ProQ2/list_stage_2.txt)
do
    if [ ! -f $x.ProQ3 ]; then
        echo "ERROR: $x.ProQ3 does not exist"
        continue
    fi

    # initialize files with scores
    if [ ! -f $scores_filename ]; then
        printf "%s    File\n" "$(head -1 $x.ProQ3)" > $scores_filename
    fi

    # pick the highest score
    scores=$(cat $x.ProQ3 | tail -1)
    printf "%s    %s\n" "$scores" $x >> $scores_filename


    if [ ! -f $x.ProQ3repacked ]; then
        echo "ERROR: $x.ProQ3repacked does not exist"
        continue
    fi

    # initialize files with scores
    if [ ! -f $refine_scores_filename ]; then
        printf "%s    File\n" "$(head -1 $x.ProQ3repacked)" > $refine_scores_filename
    fi

    # pick the highest score
    scores=$(cat $x.ProQ3repacked | tail -1)
    printf "%s    %s\n" "$scores" $x >> $refine_scores_filename

done
