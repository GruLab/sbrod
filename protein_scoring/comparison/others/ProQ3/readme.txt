1. Download the repository:
git clone https://bitbucket.org/ElofssonLab/proq3.git
Set up proq3 according to the corresponding instructions.

2. Run everything for ProQ2.
See ../ProQ2/readme.txt

3. Run scripts:
./compute_scores.py
./collect_scores.sh

Now you should have scores saved in files CASP12_ProQ3.txt and CASP12_ProQ3refine.txt
