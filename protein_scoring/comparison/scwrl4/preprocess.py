#!/usr/bin/python2.7

"""
Run scwrl4 in parallel

Dependency: Scwrl4

Example:
    ./preprocess.py "CASP/data/CASP11Stage*/*/*" 8
"""


import sys
from glob import glob
import os
import commands
from multiprocessing import Pool


def compute_scores_for_structure(filename):
    return filename + ' ' + str(commands.getstatusoutput(
                './Scwrl4 -i "{}" -o "{}_scwrl"'.format(filename, filename)
            )[0])


def main():
    if len(sys.argv) != 3:
        print("Usage: {} <structures> <num_threads>".format(sys.argv[0]))
        exit(1)

    _, structures, num_threads = sys.argv

    targets = glob(structures)

    pool = Pool(processes=int(num_threads))
    results = pool.map(compute_scores_for_structure, targets)

    print '\n'.join(results)


if __name__ == '__main__':
    main()
