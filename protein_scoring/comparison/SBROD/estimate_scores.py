#!/usr/bin/python2.7

# Usage: ./estimate_scores.py "../../../datasets/CASP/data/CASP12Stage*/*" 40 > model_caspAll11.txt

import sys
from glob import glob
import os
import commands
from multiprocessing import Pool



METHOD = "./sbrod"


def compute_scores_for_structure(filename):
    return commands.getstatusoutput('{} "{}"'.format(METHOD, filename + '/*[!.txt]'))[1]


def main():
    if len(sys.argv) != 3:
        print("Usage: {} <structure_dirs> <num_threads>".format(sys.argv[0]))
        exit(1)

    _, structures, num_threads = sys.argv

    targets = glob(structures)

    pool = Pool(processes=int(num_threads))
    results = pool.map(compute_scores_for_structure, targets)

    print '\n'.join(results)


if __name__ == '__main__':
    main()
